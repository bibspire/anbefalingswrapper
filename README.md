# Anbefalingswrapper

Denne wrapper gør det muligt at kalde BibSpire- og Den Åbne Platforms anbefalingsmotorer via APIet beskrevet i <https://platform.dandigbib.org/issues/5046>, i.e. en GET-request til 
 `/api/v1/{{CLIENT-ID}}/recommendation/tag/ddb_library_dk/id/{{FAUST-NUMMER}}`.

For at køre serveren (på port 8012):

```
npm install
npm server.js
```

Herefter kan anbefalinger ses på eksempelvis <http://localhost:8012/api/v1/bibspire-wrapper1na98k13n/recommendation/tag/ddb_library_dk/id/55125686>.
(`wrapper1na98k13n` er test-token til bibspire, – ændres hvis det misbruges) 


Hvis den åbne platform skal understøttes skal følgende environment-variable sættes: `OPENPLATFORM_CLIENTID` og `OPENPLATFORM_CLIENTSECRET`
Herefter kan openplatform-anbefalinger ses på <http://localhost:8012/api/v1/openplatform-715100/recommendation/tag/ddb_library_dk/id/55125686>.


Anbefalingssystemerne er opbygget forskelligt og har derfor ikke nødvendigvis score/reason.


## Noter om det ensartede API.

Det lyder til at det ensartede API ikke er på plads endnu, så jeg vil opfordre til en fælles dialog om hvordan det ensrettede API skal se ud, så det understøtter alle use cases. Her er et par ting at være opmærksom på:

- Anbefalinger ud fra flere materialer – eksempelvis til anbefalinger ved søgeresultater, inspiration ud fra lister af materialer, inspiration til de netop viste reserverede/lånte materialer etc.
- CORS etc. så det kan kaldes direkte fra React-komponenter
- Inkludér info om bibliotek/site hvor det vises, så det eksempelvis kan tilpasses til bibliotekets indstillinger og afgrænses til bibliotekets beholdning (måske ligger dette i client-id)
- Anbefalinger af andet end faust-numre, såsom understøttelse af værker og ikke blot manifester, arrangementer på biblioteket, etc. (jeg har heller ikke implementeret dette endnu, men bibliotekerne har vist interesse for dette). Både BibSpire og OpenPlatform tager udgangspunkt i post-id, med katalog + lokal-id i stedet for blot faust.
- Mulighed for at få medsendt metadata(inkl forside) for anbefalingerne (da der ellers skal laves et ekstra round-trip til serveren for metadata-opslag i klienten hvilket går udover performance)

I mine øjne ville den rigtige løsning være at bygge videre på den åbne platforms anbefalings-API (der har eksisteret siden 2016) som retning for et ensartet API. Her er allerede understøttelse af web-klienter, og indbygget authentificering (hvis der skal adgang til brugerdata senere).
