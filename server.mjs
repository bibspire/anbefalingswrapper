import http from 'http';
import fetch from 'node-fetch';


let tokens = {};
setInterval(() => (tokens = {}), 3600 * 1000);

async function getToken(agency) {
  const clientId = process.env.OPENPLATFORM_CLIENTID;
  const clientSecret = process.env.OPENPLATFORM_CLIENTSECRET;

  agency = agency || "";
  if (tokens[agency]) return tokens[agency];

  tokens[agency] = new Promise(async (resolve) => {
    try {
      let result = await fetch("https://auth.dbc.dk/oauth/token", {
        method: "POST",
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          Authorization:
            "Basic " +
            Buffer.from(clientId + ":" + clientSecret).toString("base64"),
        },
        body: `grant_type=password&username=@${agency}&password=@${agency}`,
      });
      result = await result.json();
      if (agency && !result.access_token) {
        return resolve(await getToken(""));
      }
      return resolve(result.access_token);
    } catch (error) {
      console.error({ error });
      delete tokens[agency];
      await sleep(2000);
      resolve(getToken(agency));
    }
  });
  return tokens[agency];
}


const server = http.createServer(async (req, res) => {
  if (req.headers.origin) {
    res.setHeader("Access-Control-Allow-Origin", req.headers.origin);
    res.setHeader("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE");
    res.setHeader("Access-Control-Allow-Credentials", "true");
    res.setHeader(
      "Access-Control-Allow-Headers",
      "Authorization, Content-Type"
    );
  }

  let m = req.url.match(new RegExp("^/api/v1/(.*)/recommendation/tag/ddb_library_dk/id/(.*)$"));

  if(m) {
    let [_, clientId, faust] = m;
    let [type, token] = clientId.split('-');
    console.log(type);
    if(type === 'bibspire') {
      let result = await fetch(`https://api.bibspire.dk/v1/recommend?site=anbefalingswrapper&token=${token}&pid=870970-basis:${faust}`)
        result = await result.json();
      result = result.related.map(o => ({id: o.hasPart[0].pid.replace(/.*:/, ''), score: "", reason: ""}));
        res.end(JSON.stringify(result));
    } else if(type === 'openplatform') {
      let agency = '';
      let opToken = await getToken(agency);
      let result = await fetch(
        `https://openplatform.dbc.dk/v3/recommend?access_token=${
          opToken
        }`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({like: ["870970-basis:" + faust]}),
        }
      );
      result = await result.json();
      result = result.data.map(o => ({id: o.pid.replace(/.*:/, ''), score: o.val, reason: ""}));
        res.end(JSON.stringify(result));
    }
  }
  res.end();
});
server.listen(process.env.PORT || 8012);


